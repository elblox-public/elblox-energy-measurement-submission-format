# elblox energy measurement submission format

This repository contains the definition of the ":elblox energy measurmenet submission format" or "e2msf" for short. The format describes the standard messages accepted by :elblox marketplaces for transmitting energy measurements from energy meters.

## Format definition
You can find the formal definition and documentation file in the `e2msf_schema.json`. Additionally, the `e2msf_example.json` file provides an example of a valid message. The definition of the standard v2 is realized with a JSON schema <https://json-schema.org/>. This json schema has been made using json schema draft 07 (<http://json-schema.org/draft-07/json-schema-release-notes.html>).

## Previous versions

* `electricity-measurement-v1` is end of life (EOL) and no longer supported.
